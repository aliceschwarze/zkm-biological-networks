Slides for a contribution to the panel discussion on biological networks at the ZKM exhibition "BarabasiLab: Hidden Patterns".

More info: [https://zkm.de/en/event/2021/10/biological-networks](https://zkm.de/en/event/2021/10/biological-networks)

Video: [https://www.youtube.com/watch?v=5qbeK9OwMg4](https://www.youtube.com/watch?v=5qbeK9OwMg4)